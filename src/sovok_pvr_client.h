#pragma once
/*
 *  Copyright (C) 2013 Alex Deryskyba (alex@codesnake.com)
 *  https://bitbucket.org/codesnake/pvr.sovok.tv_xbmc_addon
 *
 *  This file is part of pvr.sovok.tv XBMC Addon.
 *
 *  Foobar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with pvr.sovok.tv XBMC Addon. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <xbmc_pvr_types.h>
#include <string>
#include "sovok_tv.h"

class CHelper_libXBMC_pvr;
class InputBuffer;

class SovokPVRClient
{
public:
    SovokPVRClient(ADDON::CHelper_libXBMC_addon *addonHelper, CHelper_libXBMC_pvr *pvrHelper,
                   const std::string &sovokLogin, const std::string &sovokPassword);
    ~SovokPVRClient();

    PVR_ERROR GetEPGForChannel(ADDON_HANDLE handle, const PVR_CHANNEL& channel, time_t iStart, time_t iEnd);

    int GetChannelGroupsAmount();
    PVR_ERROR GetChannelGroups(ADDON_HANDLE handle, bool bRadio);
    PVR_ERROR GetChannelGroupMembers(ADDON_HANDLE handle, const PVR_CHANNEL_GROUP& group);

    int GetChannelsAmount();
    PVR_ERROR GetChannels(ADDON_HANDLE handle, bool bRadio);

    bool OpenLiveStream(const PVR_CHANNEL& channel);
    void CloseLiveStream();
    int ReadLiveStream(unsigned char* pBuffer, unsigned int iBufferSize);
    long long SeekLiveStream(long long iPosition, int iWhence);
    long long PositionLiveStream();
    long long LengthLiveStream();

    bool SwitchChannel(const PVR_CHANNEL& channel);

    void SetTimeshiftEnabled(bool enable);
    bool IsTimeshiftEnabled() { return m_isTimeshiftEnabled; }

    void SetAddFavoritesGroup(bool shouldAddFavoritesGroup);
    bool ShouldAddFavoritesGroup() { return m_shouldAddFavoritesGroup; }

    void SetStreamerId(int streamerId) { m_sovokTV.SetStreamerId(streamerId); }


private:
    SovokTV m_sovokTV;
    ADDON::CHelper_libXBMC_addon *m_addonHelper;
    CHelper_libXBMC_pvr *m_pvrHelper;
    std::string m_currentURL;
    InputBuffer *m_inputBuffer;
    bool m_isTimeshiftEnabled;
    bool m_shouldAddFavoritesGroup;
};
