/*
 *  Copyright (C) 2013 Alex Deryskyba (alex@codesnake.com)
 *  https://bitbucket.org/codesnake/pvr.sovok.tv_xbmc_addon
 *
 *  This file is part of pvr.sovok.tv XBMC Addon.
 *
 *  Foobar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Foobar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with pvr.sovok.tv XBMC Addon. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <curl/curl.h>
#include <json/json.h>
#include <assert.h>
#include <algorithm>
#include "platform/threads/mutex.h"
#include "helpers.h"
#include "sovok_tv.h"

using namespace std;
using namespace ADDON;

static const int secondsPerHour = 60 * 60;

SovokTV::SovokTV(ADDON::CHelper_libXBMC_addon *addonHelper, const string &login, const string &password) :
    m_addonHelper(addonHelper),
    m_login(login),
    m_password(password),
    m_lastEpgRequestStartTime(0),
    m_lastEpgRequestEndTime(0)
{
    if (!Login(login, password))
        throw AuthFailedException();
    LoadSettings();
}

SovokTV::~SovokTV()
{
    Logout();
}

const ChannelList &SovokTV::GetChannelList()
{
    if (m_channelList.empty())
        BuildChannelAndGroupList();

    return m_channelList;
}


void SovokTV::BuildChannelAndGroupList()
{
    m_channelList.clear();
    m_groupList.clear();

    string response = CallApiFunction("channel_list");

    Json::Value jsonRoot;
    Json::Reader jsonReader;
    if (jsonReader.parse(response, jsonRoot))
    {
        const Json::Value &groups = jsonRoot["groups"];
        Json::Value::const_iterator itGroup = groups.begin();
        for(; itGroup != groups.end(); ++itGroup)
        {
            const Json::Value &channels = (*itGroup)["channels"];
            Json::Value::const_iterator itChannel = channels.begin();
            for(; itChannel != channels.end(); ++itChannel)
            {
                SovokChannel channel;
                channel.Id = stoi((*itChannel)["id"].asString().c_str());
                channel.Name = (*itChannel)["name"].asString();
                channel.IconPath = (*itChannel)["icon"].asString();
                channel.IsRadio = channel.Id > 1000;
                m_channelList[channel.Id] = channel;

                std::string groupName = (*itGroup)["name"].asString();
                SovokGroup &group = m_groupList[groupName];
                group.Channels.insert(channel.Id);
            }
        }
    }
}

EpgEntryList SovokTV::GetEpg(int channelId, time_t startTime, time_t endTime)
{
    EpgEntryList epgEntries;

    if (startTime < m_lastEpgRequestStartTime || endTime > m_lastEpgRequestEndTime)
    {
        m_epgEntries = GetEpgForAllChannels(startTime, endTime);
        m_lastEpgRequestStartTime = startTime;
        m_lastEpgRequestEndTime = endTime;
    }

    EpgEntryList::const_iterator itEpgEntry = m_epgEntries.begin();
    for (; itEpgEntry != m_epgEntries.end(); ++itEpgEntry)
    {
        if (itEpgEntry->ChannelId == channelId)
            epgEntries.push_back(*itEpgEntry);
    }

    return epgEntries;
}

EpgEntryList SovokTV::GetEpg(int channelId, time_t day)
{
    tm *time = gmtime(&day);
    char strDate[10];
    strftime(strDate, sizeof(strDate), "%d%m%y", time);

    ParamList params;
    params["cid"] = to_string(channelId);
    params["day"] = strDate;
    string response = CallApiFunction("epg", params);

    EpgEntryList epgEntries;
    Json::Value jsonRoot;
    Json::Reader jsonReader;
    if (jsonReader.parse(response, jsonRoot))
    {
        const Json::Value &jsonEpg = jsonRoot["epg"];
        Json::Value::const_iterator itJsonEpgEntry = jsonEpg.begin();
        for (; itJsonEpgEntry != jsonEpg.end(); ++itJsonEpgEntry)
        {
            SovokEpgEntry epgEntry;
            epgEntry.Title = (*itJsonEpgEntry)["progname"].asString();
            epgEntry.StartTime = (*itJsonEpgEntry)["ut_start"].asInt();
            epgEntry.EndTime = (*itJsonEpgEntry)["ut_end"].asInt();
            epgEntries.push_back(epgEntry);
        }
    }

    return epgEntries;
}

EpgEntryList SovokTV::GetEpgForAllChannels(time_t startTime, time_t endTime)
{
    EpgEntryList epgEntries;

    int totalNumberOfHours = (endTime - startTime) / secondsPerHour;
    int hoursRemaining = totalNumberOfHours;

    while (hoursRemaining > 0)
    {
        // Query EPG for max 24 hours per single request.
        int requestNumberOfHours = min(24, hoursRemaining);

        EpgEntryList epgEntries24Hours = GetEpgForAllChannelsForNHours(startTime, requestNumberOfHours);
        epgEntries.insert(epgEntries.end(), epgEntries24Hours.begin(), epgEntries24Hours.end());

        hoursRemaining -= requestNumberOfHours;
        startTime += requestNumberOfHours * secondsPerHour;
    }

    return epgEntries;
}

EpgEntryList SovokTV::GetEpgForAllChannelsForNHours(time_t startTime, short numberOfHours)
{
    // For queries over 24 hours Sovok.TV returns incomplete results.
    assert(numberOfHours > 0 && numberOfHours <= 24);

    EpgEntryList epgEntries;

    ParamList params;
    params["dtime"] = to_string(startTime);
    params["period"] = to_string(numberOfHours);
    string response = CallApiFunction("epg3", params);

    Json::Value jsonRoot;
    Json::Reader jsonReader;
    if (jsonReader.parse(response, jsonRoot))
    {
        const Json::Value &channels = jsonRoot["epg3"];
        Json::Value::const_iterator itChannel = channels.begin();
        for (; itChannel != channels.end(); ++itChannel)
        {
            const Json::Value &jsonChannelEpg = (*itChannel)["epg"];
            Json::Value::const_iterator itJsonEpgEntry1 = jsonChannelEpg.begin();
            Json::Value::const_iterator itJsonEpgEntry2 = itJsonEpgEntry1;
            itJsonEpgEntry2++;
            for (; itJsonEpgEntry2 != jsonChannelEpg.end(); ++itJsonEpgEntry1, ++itJsonEpgEntry2)
            {
                SovokEpgEntry epgEntry;
                epgEntry.ChannelId = stoi((*itChannel)["id"].asString());
                epgEntry.Title = (*itJsonEpgEntry1)["progname"].asString();
                epgEntry.Description = (*itJsonEpgEntry1)["description"].asString();
                epgEntry.StartTime = stoi((*itJsonEpgEntry1)["ut_start"].asString());
                epgEntry.EndTime = stoi((*itJsonEpgEntry2)["ut_start"].asString());
                epgEntries.push_back(epgEntry);
            }
        }
    }

    return epgEntries;
}

const GroupList &SovokTV::GetGroupList()
{
    if (m_groupList.empty())
        BuildChannelAndGroupList();

    return m_groupList;
}

string SovokTV::GetUrl(int channelId)
{
    string url;

    ParamList params;
    params["cid"] = to_string(channelId);
    string response = CallApiFunction("get_url", params);

    Json::Value jsonRoot;
    Json::Reader jsonReader;
    if (jsonReader.parse(response, jsonRoot))
    {
        url = jsonRoot["url"].asString();
        url.replace(0, 7, "http");  // replace http/ts with http
        url = url.substr(0, url.find(" ")); // trim VLC params at the end
    }

    return url;
}

FavoriteList SovokTV::GetFavorites()
{
    string response = CallApiFunction("favorites");

    FavoriteList favorites;
    Json::Value jsonRoot;
    Json::Reader jsonReader;
    if (jsonReader.parse(response, jsonRoot))
    {
        const Json::Value &jsonFavorites = jsonRoot["favorites"];
        Json::Value::const_iterator itFavorite = jsonFavorites.begin();
        for(; itFavorite != jsonFavorites.end(); ++itFavorite)
        {
            favorites.insert(stoi((*itFavorite)["channel_id"].asString().c_str()));
        }
    }

    return favorites;
}

bool SovokTV::Login(const string &login, const string &password)
{
    m_sessionCookie.clear();

    ParamList params;
    params["login"] = login;
    params["pass"] = password;

    string response = CallApiFunction("login", params);

    Json::Value jsonRoot;
    Json::Reader jsonReader;
    if (jsonReader.parse(response, jsonRoot))
    {
        string sid = jsonRoot.get("sid", "").asString();
        string sidName = jsonRoot.get("sid_name", "").asString();

        if (!sid.empty() && !sidName.empty())
        {
            m_sessionCookie[sidName] = sid;
            return true;
        }
    }

    return false;
}

void SovokTV::Logout()
{
    CallApiFunction("logout");
}

string SovokTV::CallApiFunction(const string &name, const ParamList &params)
{
    string query;
    ParamList::const_iterator itParam = params.begin();
    for (; itParam != params.end(); ++itParam)
    {
        query += itParam == params.begin() ? "?" : "&";
        query += itParam->first + '=' + itParam->second;
    }
    string response = SendHttpRequest("http://api.sovok.tv/v2.2/json/" + name + query, m_sessionCookie);

    // In case of error try to re-login and repeat the API call.
    Json::Value jsonRoot;
    Json::Reader jsonReader;
    if (jsonReader.parse(response, jsonRoot))
    {
        if (jsonRoot.isObject() && jsonRoot.isMember("error"))
        {
            Login(m_login, m_password);
            return SendHttpRequest("http://api.sovok.tv/v2.2/json/" + name + query, m_sessionCookie);
        }
    }

    return response;
}

string SovokTV::SendHttpRequest(const string &url, const ParamList &cookie) const
{
    string response;
    char *errorMessage[CURL_ERROR_SIZE];

    CURL *curl = curl_easy_init();
    if (curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorMessage);

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CurlWriteData);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);

        string cookieStr;
        ParamList::const_iterator itCookie = cookie.begin();
        for(; itCookie != cookie.end(); ++itCookie)
        {
            if (itCookie != cookie.begin())
                cookieStr += "; ";
            cookieStr += itCookie->first + "=" + itCookie->second;
        }
        curl_easy_setopt(curl, CURLOPT_COOKIE, cookieStr.c_str());

        long httpCode = 0;
        int retries = 5;
        while (retries > 0)
        {
            CURLcode curlCode = curl_easy_perform(curl);
            if (curlCode != CURLE_OK)
            {
                m_addonHelper->Log(LOG_ERROR, "%s: %s", __FUNCTION__, errorMessage);
                break;
            }

            curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);

            if (httpCode != 503) // temporarily unavailable
                break;

            PLATFORM::CEvent::Sleep(1000);
        }

        if (httpCode != 200)
            response = "";

        curl_easy_cleanup(curl);
    }

    return response;
}

size_t SovokTV::CurlWriteData(void *buffer, size_t size, size_t nmemb, void *userp)
{
    string *response = (string *)userp;
    response->append((char *)buffer, size * nmemb);
    return size * nmemb;
}

void SovokTV::LoadSettings()
{
    std::string response = CallApiFunction("settings");

    Json::Value jsonRoot;
    Json::Reader jsonReader;
    if (jsonReader.parse(response, jsonRoot))
    {
        const Json::Value &jsonSettings = jsonRoot["settings"];
        m_streamerId = stoi(jsonSettings["streamer"].asString().c_str());
    }
}

void SovokTV::SetStreamerId(int streamerId)
{
    if (m_streamerId != streamerId)
    {
        m_streamerId = streamerId;
        ParamList params;
        params["streamer"] = to_string(m_streamerId);
        CallApiFunction("settings_set", params);
    }
}
